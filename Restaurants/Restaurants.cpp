// Restaurants.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <time.h>

using namespace std;


/*   Test Cases

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Arby's, Brick Oven, Texas Roadhouse, Chick-fil-A, Red Lobster, Chuck-a-
Rama, Taco Amigo

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
2

What restaurant do you want to add?
Arby's

Arby's is already in the list

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
3

What restaurant do you want to remove?
Brick Oven

Brick Oven has been removed from the list

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Arby's, Texas Roadhouse, Chick-fil-A, Red Lobster, Chuck-a-Rama, Taco A
migo

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
5


Please enter or delete restaurants until the number of restaurants is a power of
2

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
2

What restaurant do you want to add?
Nothing fun

Nothing fun has been added to the list

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Arby's, Texas Roadhouse, Chick-fil-A, Red Lobster, Chuck-a-Rama, Taco A
migo, Nothing fun

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
6

Goodbye
Press any key to continue . . .





1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
5

Match 1/4, Round 1/3 --- Tucanos or Chick-fil-A? Tucanos
Match 2/4, Round 1/3 --- Arby's or Red Lobster? Red Lobster
Match 3/4, Round 1/3 --- Brick Oven or Chuck-a-Rama? Brick Oven
Match 4/4, Round 1/3 --- Texas Roadhouse or Taco Amigo? Taco Amigo


Match 1/2, Round 2/3 --- Tucanos or Brick Oven? Brick OVen

Please choose one of the two restaurants
Brick Oven
Match 2/2, Round 2/3 --- Red Lobster or Taco Amigo? Red Lobster


Match 1/1, Round 3/3 --- Brick Oven or Red Lobster? Brick Oven


Brick Oven has won!
Press any key to continue . . .





1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Arby's, Brick Oven, Texas Roadhouse, Chick-fil-A, Red Lobster, Chuck-a-
Rama, Taco Amigo

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
4


The list has been shuffled

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Taco Amigo, Chuck-a-Rama, Arby's, Chick-fil-A, Texas Roadhouse, Brick O
ven, Red Lobster

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
3

What restaurant do you want to remove?
Texas Roadhouse

Texas Roadhouse has been removed from the list

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
1

Tucanos, Taco Amigo, Chuck-a-Rama, Arby's, Chick-fil-A, Brick Oven, Red Lobster

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
4


The list has been shuffled

1. Display all restaurants
2. Add a restaurant
3. Remove a restaurant
4. Shuffle the vector
5. Begin the tournament
6. Quit Program
6

Goodbye
Press any key to continue . . .
*/
bool findIfRestaurantsIsPowOfTwo(int sizeOfVector)
{
	bool isPowOfTwo = !(sizeOfVector == 0) && !(sizeOfVector & (sizeOfVector - 1));
	return isPowOfTwo;
}
int findRestaurant(string restaurantName, vector<string> restaurants)
{
	int index = -1;
	// Transform both name we are looking for and restaurants we have in the list to lowercase (so we don't have to worry about caps)
	transform(restaurantName.begin(), restaurantName.end(), restaurantName.begin(), ::tolower);
	for (int i = 0; i < restaurants.size(); i++)
	{
		transform(restaurants[i].begin(), restaurants[i].end(), restaurants[i].begin(), ::tolower);
		// See if given name exists in the vector already
		if (restaurants[i].compare(restaurantName) == 0)
		{
			// If it does, return the index of its location
			index = i;
		}
	}
	return index;
}

void addRestaurant(vector<string> &restaurants)
{
	cout << "What restaurant do you want to add?" << endl;
	string restaurantName = "";
	std::getline(cin, restaurantName);
	// See if the restaurant is already in the vector
	int indexOfRestaurant = findRestaurant(restaurantName, restaurants);
	// Add it to the vector if it isn't already there
	if (indexOfRestaurant == -1)
	{
		restaurants.push_back(restaurantName);
		cout << "\n" << restaurantName << " has been added to the list" << endl << endl;
	}
	else
	{
		// Tell the user if the restaurant is already in the list
		cout << "\n" << restaurantName << " is already in the list" << endl << endl;
	}
}
void removeRestaurant(vector<string> &restaurants)
{
	cout << "What restaurant do you want to remove?" << endl;
	string restaurantName = "";
	std::getline(cin, restaurantName);
	// See if the restaurant is already in the vector
	int indexOfRestaurant = findRestaurant(restaurantName, restaurants);
	// Tells the user if the restaurant is in the list
	if (indexOfRestaurant == -1)
	{
		cout << "\n" << restaurantName << " isn't in the list" << endl << endl;
	}
	else
	{
		// Tell the user that the restaurant has been removed
		restaurants.erase(restaurants.begin() + indexOfRestaurant);
		cout << "\n" << restaurantName << " has been removed from the list" << endl << endl;
	}
}
void shuffleRestaurants(vector<string> &restaurants)
{
	// Initialize random seed
	srand(time(0));
	for (int i = 0; i < restaurants.size(); i++)
	{
		// Randomly pick an index
		int randomIndex = rand() % restaurants.size();
		// Switch the current iteration with the random index
		string placeHolder = restaurants[i];
		restaurants[i] = restaurants[randomIndex];
		restaurants[randomIndex] = placeHolder;
	}
	cout << "\nThe list has been shuffled" << endl << endl;
}

bool beginTournament(vector<string> &restaurants)
{
	bool finishedTournament = false;
	// Make sure we have an even number of restaurants
	if (findIfRestaurantsIsPowOfTwo(restaurants.size()))
	{
		// Find out how many comparisons we have to run
		// This is how many for a round
		int numbOfMatchesForThisRound = restaurants.size() / 2;
		// Find how many rounds there are
		int numbOfRounds = 1;
		// Use this to find the number of rounds
		int disposableNumbOfMatches = numbOfMatchesForThisRound;
		while (disposableNumbOfMatches != 1)
		{
			disposableNumbOfMatches /= 2;
			numbOfRounds++;
		}
		int roundNumber = 1;
		while (roundNumber <= numbOfRounds)
		{
			int matchNumber = 1;
			// Only check as much as the vector as we have matches
			// This is so we can just ignore the restaurants that lost
			// By moving the ones that won to be in the front (or leave them there if they are already in the front)
			for (int i = 0; i < numbOfMatchesForThisRound; i++)
			{
				// Print what round and match we are on
				cout << "Match " << matchNumber << "/" << numbOfMatchesForThisRound << ", Round " << roundNumber << "/" << numbOfRounds;
				// Print out the choices
				cout << " --- " << restaurants[i] << " or " << restaurants[i + numbOfMatchesForThisRound] << "? ";
				string userChoice;
				while ((std::getline(cin, userChoice)))
				{
					// If the first choice won
					if (userChoice.compare(restaurants[i]) == 0)
					{
						// Move then leave it in the front of the vector
						break;
					}
					// If second choice won
					else if (userChoice.compare(restaurants[i + numbOfMatchesForThisRound]) == 0)
					{
						// Then swap it with the first choice so the second choice is in the front of the vector
						string placeHolder = restaurants[i];
						restaurants[i] = restaurants[i + numbOfMatchesForThisRound];
						restaurants[i + numbOfMatchesForThisRound] = placeHolder;
						break;
					}
					else
					{
						cout << "\nPlease choose one of the two restaurants" << endl;

					}
				}
				matchNumber++;
			}
			cout << endl << endl;
			roundNumber++;
			numbOfMatchesForThisRound /= 2;
			// Set boolean to true so we can exit the loop in the main

		}
		// The winning string will always be at the first position
		cout << restaurants[0] << " has won!" << endl;
		finishedTournament = true;
	}
	else
	{
		cout << "\nPlease enter or delete restaurants until the number of restaurants is a power of 2" << endl << endl;
	}
	return finishedTournament;
}

int main()
{
	vector<string> restaurants;
	// Insert the initial restaurants
	restaurants.push_back("Tucanos");
	restaurants.push_back("Arby's");
	restaurants.push_back("Brick Oven");
	restaurants.push_back("Texas Roadhouse");
	restaurants.push_back("Chick-fil-A");
	restaurants.push_back("Red Lobster");
	restaurants.push_back("Chuck-a-Rama");
	restaurants.push_back("Taco Amigo");

	// Keeps track of whether we should exit loop
	bool quitTournament = false;

	while (!quitTournament)
	{
		// Main menu
		cout << "1. Display all restaurants" << endl;
		cout << "2. Add a restaurant" << endl;
		cout << "3. Remove a restaurant" << endl;
		cout << "4. Shuffle the vector" << endl;
		cout << "5. Begin the tournament" << endl;
		cout << "6. Quit Program" << endl;
		int menuInput;
		if (!(cin >> menuInput))
		{
			cin.clear();
			cout << "Please choose a valid menu option using its labeled number" << endl;
			menuInput = -1;
		}
		cin.ignore(1000, '\n');
		cout << endl;
		if (menuInput == 1)
		{
			for (int i = 0; i < restaurants.size(); i++)
			{
				if (i == restaurants.size() - 1)
				{
					cout << restaurants[i] << endl << endl;
				}
				else
				{
					cout << restaurants[i] << ", ";
				}

			}
		}
		else if (menuInput == 2)
		{
			addRestaurant(restaurants);
		}
		else if (menuInput == 3)
		{
			removeRestaurant(restaurants);
		}
		else if (menuInput == 4)
		{
			shuffleRestaurants(restaurants);
		}
		else if (menuInput == 5)
		{
			// See if we should quit the loop
			quitTournament = beginTournament(restaurants);
		}
		else if (menuInput == 6)
		{
			cout << "Goodbye" << endl;
			break;
		}
		else
		{

		}
	}



	system("pause");
	return 0;
}

